# Load Graph and Semantic Rules file Upload 

## Description
This is the API used for the instantiation of the Knowledge Graph into Neo4j.

## Usage
After cloning the project on your local machine/server, type:
- cd knowledge-graph-api
- docker-compose up --build

It provides the following features through REST API endpoints: 

### 1. Creating the Graph
For uploading the .json file exported from CESE Framework 
(https://risk-mgmt.ait.ac.at/dev-precinct/#/network) to create the knowledge graph into neo4j, open an internet browser and go to
the url: http://localhost:3061/upload_graph_file. The following page will show up, where you can choose the file to 
upload (visual 1). 

Visual 1: Screen for uploading the .json file exported from CESE
![](pics/upload_cese_file.png "upload_sauron")

After choosing the .json file, click on "Create Graph in Neo4j" and upon successful completion, the following messages will 
appear:
- Graph is created

To check if the graph is created in Neo4j, go to the url: http://localhost:3060/browser/ and login with credentials: 
Username: neo4j / Password: neo4j_user . Then execute the query "MATCH (N) RETURN N", on the command line. The graph 
will appear as shown in visual 2.

Visual 2: Knowledge Graph in Neo4j after the upload of CESE file
![](pics/kg2.png "knowledge-graph1")


### 2. Defining the Semantic Rules
To upload the file that contains the semantic rules, go to the url: http://localhost:3061/upload_semantic_rules.
The following page will show up, where you can choose the file to upload (visual 3). 

Visual 3: Screen for uploading the .ttl file containing the semantic rules
![](pics/upload_semantic_rules1.png "upload_semantic")

After choosing the .ttl file, click on "Upload Semantic Rules File" and upon successful completion, the following message 
will appear: "File successfully Uploaded"

* For the format of the file, please refer to the file (validations.ttl) in the "examples" folder.

### 3. Creating or Updating the properties of the nodes in the graph
To create or update nodes' properties by uploading a file (.csv, .json), that contains the data, go to the url: 
http://localhost:3061/upload_data_file.
The following page will show up, where you can choose the file to upload (visual 4).

Visual 4: Screen for uploading a data file to create or update nodes' properties
![](pics/upload_datafile.png "upload_data")

* For the format of the files, please refer to the files (coord_data.csv and json_test_data.json) 
in the "examples" folder.


### 4. Exporting the KG
To export and download the knowledge graph into a GRAPHML formatted file, go to the url: http://localhost:3061/database.
The following page will show up, where you can click on the _Download_ button and save the .graphml file in your host machine
(visual 5).

Visual 5: Screen for exporting & downloading the KG
![](pics/export_graph1.png "export-graph")

### 5. GET Requests

- For obtaining all the properties of a specific node of the graph, send the following GET request: 

http://localhost:3061/getdata/<label_of_the_node>

*for example: http://localhost:3061/getdata/MS_Koropi

- For obtaining a specific property of a node of the graph, send the following GET request: 

http://localhost:3061/getvalue/<label_of_the_node>/<name_of_property>

*for example: http://localhost:3061/getvalue/MS_Koropi/state

 - Use the url: http://localhost:3061/get_triggers to get the content of the triggers being added in the graph

 - Use the url: http://localhost:3061/get_semantic_rules to get the content of the semantic rules being added 
 in the graph

### 6. Importing Infrastructural data in the nodes of the graph
To import the infrastructural data by uploading the file (.json), that contains the data in the specific format, 
go to the url: 
http://localhost:3061/infrastructural_data.

*The filed "label" of the json structure must be a valid node label in the graph.
The following page will show up, where you can choose the file to import (visual 6).

Visual 6: Screen for uploading a data file to import infrastructural data
![](pics/upload_infrastructural_data.png "infrastructural_data")

* For the format of the files, please refer to the file (infra_metroStations.json) in the "examples" folder.

### 7. Importing the GRAPHML Backup file into a new instance of Neo4j
To import the backup data by uploading the file (.graphml), go to the url: 
http://localhost:3061/import_backup

Visual 7: Screen for uploading the backup data file (.graphml) to import into Neo4j
![](pics/upload_backup.png "export-graph")

### 8. Defining the triggers of the current state 
To import the triggers, that change the current state of each node, by uploading the file (.json), go to the url: 
http://localhost:3061/upload_triggers

Visual 8: Screen for uploading the file containing the triggers of the nodes
![](pics/upload_triggers.png "upload-triggers")

* For the format of the upload file, please refer to the file (States_alerts.xlsx) in the "examples" folder.
* To test the triggers, use the file "json_test_data.json" located in the "examples" folder to 
the http://localhost:3061/upload_data_file 

* Please note that a POST request with a json object (identical with the one contained in the "json_test_data.json" file) 
can be sent to the following endpoint http://localhost:3061/update_data_json for the properties to be updated

## Support
For any further assistance or clarifications, please contact eleni.voulgari@konnecta.io